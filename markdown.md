# Markdown markup testing
<a href="#anchor"></a>There is a hidden \<a href="#anchor"\>\</a\> anchor element here.    

## Basic markup
*This sentence should be italic.* It has a single \* character at the beginning and at the end.  
_This sentence should be italic._ It has a single \_ character at the beginning and at the end.  

**This sentence should be bold.** It has double \* character at the beginning and at the end.  
__This sentence should be bold.__ It has double \_ character at the beginning and at the end.  

This sentence should be right above the next one.<br>
This is because there is a \<br\> element at the end of the previous sentence.
This  shoud be in the same line, because it only has a 'crlf'(enter) at the end of the previous sentence.  
However, this one should be in a new line, because there is two whitespaces at the end of the previous one.  



## Links, anchors
[This should lead to the Basic Markup section](#basic-markup)  
[This should lead to the anchor element](#anchor)  

## HTML Markup
<a href="#anchor">This link should reference to the #anchor at the beginning of the document</a>
